#
# HomeMaster Driver 编译
#

obj-m += homemaster.o
homemaster-objs := master.o platform.o platform_ht.o
homemaster-objs += platform_light.o platform_oled.o platform_infrared.o

PWD=$(shell pwd)

export OPENWRT = /Volumes/Developer/widora/openwrt
export STAGING_DIR = ${OPENWRT}/staging_dir/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/bin
export KERNEL_DIR = ${OPENWRT}/build_dir/target-mipsel_24kec+dsp_uClibc-0.9.33.2/linux-ramips_mt7688/linux-3.18.29
export LD = ${OPENWRT}/staging_dir/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mipsel-openwrt-linux-uclibc-ld
export TOOLCHAIN = ${OPENWRT}/staging_dir/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mipsel-openwrt-linux-uclibc-
export CC = ${OPENWRT}/staging_dir/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mipsel-openwrt-linux-uclibc-gcc

all:
	make -C $(KERNEL_DIR) ARCH=mips \
		CROSS_COMPILE=$(TOOLCHAIN) \
		M=$(PWD) \
		modules
	@make clean_tmp

remote:
	scp -r homemaster.ko root@openwrt.local:~


clean_tmp:
	@rm -f *.o
	@rm -f .*.cmd
	@rm -rf *.mod
	@rm -f *.mod.c
	@rm -rf .tmp_versions
	@rm -f *.order
	@rm -f *.symvers

clean:
	rm -f *.ko
	rm -f *.o
	rm -f .*.cmd
	rm -rf *.mod
	rm -f *.mod.c
	rm -rf .tmp_versions
	rm -f *.order
	rm -f *.symvers
