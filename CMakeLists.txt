#
# 内驱驱动模块部分
# 此文件只是为了在CLion编辑时关联头文件之用
#

project(homemaster-driver)

cmake_minimum_required(VERSION 3.4)

# 内核模块引入头文件部分
add_definitions(-D__KERNEL__)
add_definitions(-DCONFIG_GPIOLIB)
add_definitions(-DEDIT_KERNEL)
add_definitions(-DCONFIG_I2C_MODULE)

include_directories(/Volumes/Developer/widora/openwrt/build_dir/target-mipsel_24kec+dsp_uClibc-0.9.33.2/linux-ramips_mt7688/linux-3.18.29/include/)
include_directories(/Volumes/Developer/widora/openwrt/build_dir/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/linux-3.18.29/include/)
include_directories(/Volumes/Developer/widora/sdk/staging_dir/target-mipsel_24kec+dsp_uClibc-0.9.33.2/usr/include)
include_directories(/Volumes/Developer/widora/sdk/staging_dir/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/include)

# 将原先的小块驱动做一次集成
set(KERNEL_MASTER
        master.c
        master.h
        platform.c
        platform.h
        platform_radio.c
        platform_radio.h
        platform_ht.c
        platform_ht.h
        platform_light.c
        platform_light.h
        platform_radio_reg.h
        platform_oled.c
        platform_oled.h
        platform_light_reg.h
        platform_infrared.c
        platform_infrared.h)
add_executable(master_driver_ko ${KERNEL_MASTER})




