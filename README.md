# HomeMaster-Driver
HomeMaster网关驱动部分

## 智能家居DIY项目 v2018版
该项目属于智能家居定制项目的一部分，该项目还包括:

- [**homemaster 设备接入网关, 采用Go+C开发**](https://gitee.com/adaidesigner/homemaster)
- [**homemaster-driver 网关硬件设备驱动, 由C编写的linux内核模块**](https://gitee.com/adaidesigner/homemaster-driver)
- [**zigbee3.0-coordinator 网关协调器部分, 芯片采用NXP的JN5169**](https://gitee.com/adaidesigner/zigbee3.0-coordinator)
- [**jarvis 家庭控制中枢服务端部分, 采用Go编写**](https://gitee.com/adaidesigner/jarvis)
`取名来源: JARVIS(贾维斯)钢铁侠托尼的AI助理，幻视的核心软件`
- [**home 家居ios端应用程序,兼容iphone和ipad, 由swift编写 (半成品,未完工)**](https://gitee.com/adaidesigner/home)

## 智能家居定制项目介绍网址: [**https://adai.design/design**](http://adai.design/design)

----

## 驱动说明
[HomeMaster PCB原理图](homemaster.pdf)
![](hardware.png)

## PCB制板3D外观
所有元器件都有3D模型，方便导入CreoParameteric进行装配
Altium Designer PCB3D模型截图
![](altiumdesigner.png)

## 智能家居定制项目介绍网址: [**https://adai.design/design**](http://adai.design/design)
